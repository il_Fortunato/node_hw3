const dotenv = require('dotenv').config({ path: './.env' });
const fs = require('fs');
const path = require('path');
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const { authRouter } = require('./routers/authRouter');
const { truckRouter } = require('./routers/truckRouter');
const { loadRouter } = require('./routers/loadRouter');
const { userRouter } = require('./routers/userRouter');

const PORT = process.env.PORT;
const CONNECT_LINK = process.env.DB_CONNECT_LINK;

mongoose.connect(CONNECT_LINK);

const logger = fs.createWriteStream(path.join(__dirname, 'history.log'), { flags: 'a' });

const errorHandler = (err, req, res, next) => {
  console.error(err);
  if (!err.statusCode) err.statusCode = 400;
  res.status(err.statusCode)
    .send({
      message: err.message,
    });
};

const start = () => {
  try {
    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

app.use(express.json());
app.use(morgan('combined', { stream: logger }));
app.use('/api/auth', authRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/users', userRouter);
app.use('/api/loads', loadRouter);
app.use(errorHandler);

start();

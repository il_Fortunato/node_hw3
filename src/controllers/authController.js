const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const {
  Credentials,
  credJoi
} = require('../models/Credentials');
const {
  User,
  userJoi
} = require('../models/User');
const {
  regCredentials,
  regCredJoi
} = require('../models/registrationCredentials');

const registerUser = async (req, res, next) => {
  const {
    email,
    password,
    role
  } = req.body;

  const encryptedPassword = await bcrypt.hash(password, 10);

  await credJoi.validateAsync({
    email,
    password
  });
  await userJoi.validateAsync({
    role,
    email
  });
  await regCredJoi.validateAsync({
    email,
    password,
    role
  });

  const credentials = new Credentials({
    email,
    password: encryptedPassword,
  });

  const regCred = new regCredentials({
    _id: credentials.id,
    email,
    password: encryptedPassword,
    role,
  });

  const user = new User({
    _id: credentials.id,
    role,
    email,
    createdDate: Date.now(),
  });


  user.save();
  regCred.save();
  credentials.save()
    .then(() =>
      res.status(200)
        .send({
          message: 'Profile created successfully',
        }));
};

const loginUser = async (req, res, next) => {
  const user = await Credentials.findOne({ email: req.body.email });

  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = {
      email: user.email,
      userId: user.id
    };

    const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);

    res.status(200)
      .send({
        message: 'Success',
        jwt_token: jwtToken,
      });
  } else {
    next({
      message: 'Not authorized',
      statusCode: 400,
    });
  }
};

const forgotPassword = async (req, res, next) => {
  const { email } = req.body;

  const user = await Credentials.findOne({ email: email });

  const encryptedNewPass = await bcrypt.hash('123', 10);

  if (!user) {
    next({
      message: 'Such user doesn\'t exist',
      statusCode: 400
    });
    return;
  }

  const account = {
    user: 'sweest2000@gmail.com',
    pass: 'dzjvssqhruduahxq'
  }

  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: account.user, // generated ethereal user
      pass: account.pass, // generated ethereal password
    },
  });
  await transporter.sendMail({
    from: `${account.user}`, // sender address
    to: `${email}`, // list of receivers
    subject: 'Your new password', // Subject line
    text: '123', // plain text body
  });

  await Credentials.findByIdAndUpdate(user.id, {
    password: encryptedNewPass
  });
  await regCredentials.findByIdAndUpdate(user.id, {
    password: encryptedNewPass
  })
    .then(() => {
      res.status(200)
        .send({
          message: 'New password sent to your email address',
        });
    });
};

module.exports = {
  registerUser,
  loginUser,
  forgotPassword,
};

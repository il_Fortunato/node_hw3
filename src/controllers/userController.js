const bcrypt = require('bcryptjs');
const {
  Credentials
} = require('../models/Credentials');
const {
  User
} = require('../models/User');
const {
  regCredentials
} = require('../models/registrationCredentials');

const getUser = async (req, res, next) => {
  const user = await User.findOne({ _id: req.user.userId });

  if (!user) {
    next({
      message: 'Bad request',
      statusCode: 400,
    });
  }

  res.status(200)
    .send({
      user: {
        _id: user.id,
        role: user.role,
        email: user.email,
        createdDate: user.created_date,
      },
    });
};

const deleteUser = async (req, res, next) => {
  await Credentials.findByIdAndDelete(req.user.userId);
  await regCredentials.findByIdAndDelete(req.user.userId);
  await User.findByIdAndDelete(req.user.userId)
    .then(() =>
      res.status(200)
        .send({
          message: 'Success',
        }));
};

const changePassword = async (req, res, next) => {
  const {
    oldPassword,
    newPassword,
  } = req.body;

  const user = await Credentials.findOne({ _id: req.user.userId });

  if (!user) {
    next({
      message: 'Bad request',
      statusCode: 400,
    });
  }

  const matchPasswords = await bcrypt.compare(oldPassword, user.password);

  if (!matchPasswords) {
    next({
      message: 'Old password is incorrect',
      statusCode: 400,
    });
    return;
  }

  const newEncrypted = await bcrypt.hash(newPassword, 10);

  await regCredentials.findByIdAndUpdate(req.user.userId, {
    password: newEncrypted
  });
  await Credentials.findByIdAndUpdate(req.user.userId, {
    password: newEncrypted
  })
    .then(() =>
      res.status(200)
        .send({
          message: 'Success',
        }));
};

module.exports = {
  getUser,
  deleteUser,
  changePassword
};

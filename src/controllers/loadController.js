const {
  Load,
  loadJoi
} = require('../models/Load');

const {
  Truck
} = require('../models/Truck');

const {
  User
} = require('../models/User');

let currentState = 0;
const states = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'];
const truckTypes = {
  'SPRINTER': {
    payload: 1700,
    dimensions: {
      width: 300,
      length: 250,
      height: 170
    }
  },
  'SMALL STRAIGHT': {
    payload: 2500,
    dimensions: {
      width: 500,
      length: 250,
      height: 170
    }
  },
  'LARGE STRAIGHT': {
    payload: 4000,
    dimensions: {
      width: 700,
      length: 350,
      height: 200
    }
  }
};

const getLoads = async (req, res, next) => {
  const {
    limit = 100,
    offset = 0
  } = req.body;

  const user = await User.findOne({ _id: req.user.userId });

  if (user.role === 'DRIVER') {
    const loads = await Load.find({
      status: {
        $in: ['ASSIGNED', 'SHIPPED']
      }
    })
      .skip(offset)
      .limit(limit);

    res.status(200)
      .send({
        loads
      });
  } else {
    const loads = await Load.find({
      status: {
        $in: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED']
      }
    })
      .skip(offset)
      .limit(limit);

    res.status(200)
      .send({
        loads
      });
  }
};

const addLoad = async (req, res, next) => {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions
  } = req.body;
  const user = await User.findOne({ _id: req.user.userId });

  await loadJoi.validateAsync({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions
  });

  if (user.role !== 'DRIVER') {
    const load = new Load({
      created_by: req.user.userId,
      assigned_to: 'none',
      status: 'NEW',
      state: 'none',
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions
    });
    load.save()
      .then(() => {
        res.status(200)
          .send({
            message: 'Load created successfully'
          });
      });
  } else {
    next({
      message: 'Driver is not allowed to add loads',
      statusCode: 400
    });
  }
};

const getActiveLoad = async (req, res, next) => {
  const user = await User.findOne({ _id: req.user.userId });

  if (user.role !== 'DRIVER') {
    next({
      message: 'Only available for drivers',
      statusCode: 400
    });
  }

  const truck = await Truck.findOne({ assigned_to: req.user.userId });
  const load = await Load.findOne({ assigned_to: truck._id });

  if (!load) {
    next({
      message: 'No active loads for current user',
      statusCode: 400
    });
    return;
  }

  res.status(200)
    .send({
      load
    });
};

const setNextState = async (req, res, next) => {
  const user = await User.findOne({ _id: req.user.userId });

  if (user.role !== 'DRIVER') {
    next({
      message: 'Available only for drivers',
      statusCode: 400
    });
    return;
  }

  const currentTruck = await Truck.findOne({ assigned_to: req.user.userId });
  const load = await Load.findOne({ assigned_to: currentTruck._id });

  if (!load) {
    next({
      message: 'No active loads for current user',
      statusCode: 400
    });
    return;
  }

  if (load.state === states[states.length - 1]) {
    next({
      message: 'Load is already delivered',
      statusCode: 400
    });
    return;
  }

  if (load.state === states[currentState]) {
    load.state = states[currentState + 1];
    currentState++;
    if (load.state === states[states.length - 1]) {
      load.status = 'SHIPPED';
      currentTruck.status = 'IS';
    }
    load.logs.push({
      message: `Load state changed to ${load.state}`,
      time: new Date(Date.now()).toISOString()
    });
  }

  load.save();
  currentTruck.save()
    .then(() => {
      res.status(200)
        .send({
          message: `Load state changed to ${load.state}`
        });
    });
};

const getLoad = async (req, res, next) => {
  const { id } = req.params;

  const load = await Load.findOne({ _id: id });

  res.status(200)
    .send({
      load
    });
};

const updateLoad = async (req, res, next) => {
  const { id } = req.params;
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions
  } = req.body;

  const user = await User.findOne({ _id: req.user.userId });

  if (user.role !== 'SHIPPER') {
    next({
      message: 'Available only for shipper',
      statusCode: 400
    });
    return;
  }

  await Load.findByIdAndUpdate(id, {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions
  })
    .then(() => {
      res.status(200)
        .send({
          message: 'Load details changed successfully'
        });
    });
};

const deleteLoad = async (req, res, next) => {
  const { id } = req.params;

  const user = await User.findOne({ _id: req.user.userId });

  if (user.role !== 'SHIPPER') {
    next({
      message: 'Available only for shipper',
      statusCode: 400
    });
    return;
  }

  await Load.findByIdAndDelete(id)
    .then(() => {
      res.status(200)
        .send({
          message: 'Load deleted successfully'
        });
    });
};

const postLoad = async (req, res, next) => {
  const { id } = req.params;

  const user = await User.findOne({ _id: req.user.userId });

  if (user.role !== 'SHIPPER') {
    next({
      message: 'Available only for shipper',
      statusCode: 400
    });
    return;
  }

  const load = await Load.findOne({ _id: id });
  const availableTruck = await Truck.findOne({
    assigned_to: {
      $ne: 'none'
    },
    status: 'IS',
  });

  if (!availableTruck) {
    next({
      message: 'No truck available',
      statusCode: 400
    });
    return;
  }

  if (load.payload > truckTypes[`${availableTruck.type}`].payload
    || load.dimensions.width > truckTypes[`${availableTruck.type}`].width
    || load.dimensions.length > truckTypes[`${availableTruck.type}`].length
    || load.dimensions.height > truckTypes[`${availableTruck.type}`].height) {
    next({
      message: 'Load doesn\'t fit in available truck',
      statusCode: 400
    });
    return;
  }

  load.status = 'POSTED';
  load.assigned_to = availableTruck._id;
  load.status = 'ASSIGNED';
  load.state = states[0];
  load.logs.push({
    message: `Load signed to driver with id ${req.user.userId}`,
    time: new Date(Date.now()).toISOString()
  });
  availableTruck.status = 'OL';

  load.save();
  availableTruck.save()
    .then(() => {
      res.status(200)
        .send({
          message: 'Load posted successfully',
          driver_found: true
        });
    });
};

const getShippingInfo = async (req, res, next) => {
  const { id } = req.params;

  const user = await User.findOne({ _id: req.user.userId });

  if (user.role !== 'SHIPPER') {
    next({
      message: 'Available only for shipper',
      statusCode: 400
    });
    return;
  }

  const load = await Load.findOne({ _id: id });

  if (!load) {
    next({
      message: 'Load not found',
      statusCode: 400
    });
  }

  const truck = await Truck.findOne({ _id: load.assigned_to });

  if (!truck) {
    next({
      message: 'No such info available',
      statusCode: 400
    });
    return;
  }

  res.status(200)
    .send({
      load,
      truck
    });
};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoad,
  setNextState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getShippingInfo
};

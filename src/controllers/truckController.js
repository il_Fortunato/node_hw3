const {
  Truck,
  truckJoi
} = require('../models/Truck');

const {
  User
} = require('../models/User');

const getTrucks = async (req, res, next) => {
  const trucks = await Truck.find({ created_by: req.user.userId });

  const user = await User.findOne({ _id: req.user.userId });

  if (user.role !== 'DRIVER') {
    next({
      message: 'Only drivers can get trucks',
      statusCode: 400
    });
    return;
  }

  res.status(200)
    .send({
      trucks
    });
};

const addTruck = async (req, res, next) => {
  const { type } = req.body;

  const user = await User.findOne({ _id: req.user.userId });

  if (user.role !== 'DRIVER') {
    next({
      message: 'Only drivers can add new trucks',
      statusCode: 400
    });
    return;
  }

  await truckJoi.validateAsync({ type });

  const truck = new Truck({
    created_by: req.user.userId,
    assigned_to: 'none',
    type,
    status: 'IS',
  });

  truck.save()
    .then(() => {
      res.status(200)
        .send({
          message: 'Truck created successfully'
        });
    });
};

const getTruck = async (req, res, next) => {
  const { id } = req.params;

  const truck = await Truck.findOne({ _id: id });

  const user = await User.findOne({ _id: req.user.userId });

  if (user.role !== 'DRIVER') {
    next({
      message: 'Only drivers can get truck',
      statusCode: 400
    });
    return;
  }

  res.status(200)
    .send({
      truck
    });
};

const updateTruck = async (req, res, next) => {
  const { id } = req.params;
  const { type } = req.body;

  await truckJoi.validateAsync({ type });

  const user = await User.findOne({ _id: req.user.userId });

  if (user.role !== 'DRIVER') {
    next({
      message: 'Only drivers can add update trucks\' info',
      statusCode: 400
    });
    return;
  }

  const currentTruck = await Truck.findOne({ assigned_to: req.user.userId });

  if (currentTruck.status === 'OL'){
    next({
      message: 'Info cannot be changed while on load',
      statusCode: 400
    });
    return;
  }

  await Truck.findByIdAndUpdate(id, {
    type
  })
    .then(() => {
      res.status(200)
        .send({
          message: 'Truck details changed successfully'
        });
    });

};
const deleteTruck = async (req, res, next) => {
  const { id } = req.params;

  const user = await User.findOne({ _id: req.user.userId });

  if (user.role !== 'DRIVER') {
    next({
      message: 'Only drivers can delete trucks',
      statusCode: 400
    });
    return;
  }

  await Truck.findByIdAndDelete(id)
    .then(() => {
      res.status(200)
        .send({
          message: 'Truck deleted successfully'
        });
    });
};

const assignTruck = async (req, res, next) => {
  const { id } = req.params;

  const truck = await Truck.findOne({ _id: id });
  const assignedTruck = await Truck.find({ assigned_to: req.user.userId });

  const user = await User.findOne({ _id: req.user.userId });

  if (user.role !== 'DRIVER') {
    next({
      message: 'Only drivers can assign trucks',
      statusCode: 400
    });
    return;
  }

  if (truck.assigned_to !== 'none'){
    next({
      message: 'Truck is already assigned',
      statusCode: 400
    });
    return;
  }

  if (assignedTruck.length){
    next({
      message: 'You already have a truck assigned',
      statusCode: 400
    });
    return;
  }

  await Truck.findByIdAndUpdate(id, {
    assigned_to: req.user.userId
  })
    .then(() => {
      res.status(200)
        .send({
          message: 'Truck assigned successfully'
        });
    });
};

module.exports = {
  getTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck
};

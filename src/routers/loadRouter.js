const express = require('express');
const router = express.Router();
const { authMiddleware } = require('../middleware/authMiddle');
const {
  getLoads,
  addLoad,
  getActiveLoad,
  setNextState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getShippingInfo
} = require('../controllers/loadController');

const asyncWrapper = (controller) => {
  return (req, res, next) => controller(req, res, next).catch(next);
}

router.get('/', authMiddleware, getLoads);

router.post('/', authMiddleware, asyncWrapper(addLoad));

router.get('/active', authMiddleware, getActiveLoad);

router.patch('/active/state', authMiddleware, setNextState);

router.get('/:id', authMiddleware, getLoad);

router.put('/:id', authMiddleware, updateLoad);

router.delete('/:id', authMiddleware, deleteLoad);

router.post('/:id/post', authMiddleware, postLoad);

router.get('/:id/shipping_info', authMiddleware, getShippingInfo);

module.exports = {
  loadRouter: router
};

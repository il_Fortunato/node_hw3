const express = require('express');
const router = express.Router();
const { authMiddleware } = require('../middleware/authMiddle');
const {
  getTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck
} = require('../controllers/truckController');

const asyncWrapper = (controller) => {
  return (req, res, next) => controller(req, res, next).catch(next);
}

router.get('/', authMiddleware, getTrucks);

router.post('/', authMiddleware, asyncWrapper(addTruck));

router.get('/:id', authMiddleware, getTruck);

router.put('/:id', authMiddleware, asyncWrapper(updateTruck));

router.delete('/:id', authMiddleware, deleteTruck);

router.post('/:id/assign', authMiddleware, assignTruck);

module.exports = {
  truckRouter: router
};

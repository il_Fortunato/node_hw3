const express = require('express');
const router = express.Router();
const { authMiddleware } = require('../middleware/authMiddle');
const { getUser, deleteUser, changePassword } = require('../controllers/userController');


router.get('/me', authMiddleware, getUser);

router.delete('/me', authMiddleware, deleteUser);

router.patch('/me/password', authMiddleware, changePassword);


module.exports = {
  userRouter: router
}

const mongoose = require('mongoose');
const Joi = require('joi');

const userJoi = Joi.object({
    role: Joi.string().alphanum().required(),
    email: Joi.string().email({ minDomainSegments: 2 }),
    created_date: Joi.date()
});

const User = mongoose.model('User', {
    role: {
        type: String,
        required: true
    },
    email: {
        type: String
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});

module.exports = {
    userJoi,
    User
}

const mongoose = require('mongoose');
const Joi = require('joi')

const regCredJoi = Joi.object({
  email: Joi.string().email({ minDomainSegments: 2 }),
  password: Joi.string().alphanum().required(),
  role: Joi.string().valid('SHIPPER', 'DRIVER').required()
});

const regCredentials = mongoose.model('registrationCredentials', {
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  }
});

module.exports = {
  regCredJoi,
  regCredentials,
};

const mongoose = require('mongoose');
const Joi = require('joi');

const truckJoi = Joi.object({
  created_by: Joi.string(),
  assigned_to: Joi.string(),
  type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'),
  status: Joi.string().valid('OL', 'IS'),
  created_date: Joi.date()
});

const Truck = mongoose.model('Truck', {
  created_by: {
    type: String,
    required: true
  },
  assigned_to: {
    type: String
  },
  type: {
    type: String
  },
  status: {
    type: String
  },
  created_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = {
  truckJoi,
  Truck
}

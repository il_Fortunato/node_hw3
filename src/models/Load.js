const mongoose = require('mongoose');
const Joi = require('joi');

const loadJoi = Joi.object({
  created_by: Joi.string(),
  assigned_to: Joi.string(),
  status: Joi.string()
    .valid('NEW', 'POSTED', 'ASSIGNED', 'DELIVERED'),
  state: Joi.string()
    .valid('En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'),
  name: Joi.string(),
  payload: Joi.number()
    .integer(),
  pickup_address: Joi.string(),
  delivery_address: Joi.string(),
  dimensions: Joi.object()
    .keys({
      width: Joi.number(),
      length: Joi.number(),
      height: Joi.number()
    }),
  logs: Joi.array()
    .items(Joi.object()
      .keys({
        message: Joi.string(),
        time: Joi.date()
      })),
  created_date: Joi.date()
});

const Load = mongoose.model('Load', {
  created_by: {
    type: String,
    required: true
  },
  assigned_to: {
    type: String,
  },
  status: {
    type: String,
  },
  state: {
    type: String,
  },
  name: {
    type: String,
  },
  payload: {
    type: Number,
  },
  pickup_address: {
    type: String,
  },
  delivery_address: {
    type: String,
  },
  dimensions: {
    type: Object,
  },
  logs: {
    type: Array,
  },
  created_date: {
    type: Date,
    default: Date.now,
  }
});

module.exports = {
  loadJoi,
  Load
};

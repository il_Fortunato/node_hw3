const mongoose = require('mongoose');
const Joi = require('joi')

const credJoi = Joi.object({
  email: Joi.string().email({ minDomainSegments: 2 }),
  password: Joi.string().alphanum().required()
});

const Credentials = mongoose.model('Credentials', {
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

module.exports = {
  credJoi,
  Credentials,
};
